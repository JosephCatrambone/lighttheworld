package com.josephcatrambone.lighttheworld;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.josephcatrambone.lighttheworld.actors.Light;

public class LevelIO {
	public static String toString(List <Light> lights, List <Polygon> obstacles) {
		// # lights
		// For each light: x/screenWidth y/screenHeight r g b a
		// # obstacles
		// For each obstacle: # verts
		//   For each vert: x/screenWidth y/screenWidth
		String out = "";
		
		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();
		
		// Write lights
		out += lights.size() + "\n";
		for(Light light : lights) {
			Color c = light.getColor();
			out += light.position.x/screenWidth + " " + light.position.y/screenHeight + " " + c.r + " " + c.g + " " + c.b + " " + c.a + "\n"; 
		}
		
		// Write obstacles
		out += obstacles.size() + "\n";
		for(Polygon p : obstacles) {
			float[] verts = p.getTransformedVertices();
			out += verts.length + "\n";
			for(int i=0; i < verts.length; i+=2) {
				out += verts[i]/screenWidth + " ";
				out += verts[i+1]/screenHeight + " ";
			}
			out += "\n";
		}
		
		return out;
	}
	
	public static void fromString(String str, List <Light> lights, List <Polygon> obstacles) {
		Scanner scanner = new Scanner(str);
		fromScanner(scanner, lights, obstacles);
		scanner.close();
	}
	
	public static void fromScanner(Scanner scanner, List <Light> lights, List <Polygon> obstacles) {
		//Light[] lights = new Light[scanner.nextInt()];
		int numLights = scanner.nextInt();
		
		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();
		
		for(int i=0; i < numLights; i++) {
			Light light = new Light();
			light.position = new Vector2(scanner.nextFloat()*screenWidth, scanner.nextFloat()*screenHeight);
			light.setColor(scanner.nextFloat(), scanner.nextFloat(), scanner.nextFloat(), scanner.nextFloat());
			lights.add(light);
		}
		
		//Polygon[] polygons = new Polygon[scanner.nextInt()];
		int numPolygons = scanner.nextInt();
		
		for(int i=0; i < numPolygons; i++) {
			float[] points = new float[scanner.nextInt()];
			for(int j=0; j < points.length; j+=2) {
				points[j] = scanner.nextFloat()*screenWidth;
				points[j+1] = scanner.nextFloat()*screenHeight;
			}
			obstacles.add(new Polygon(points));
		}
	}
}
