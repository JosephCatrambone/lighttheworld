package com.josephcatrambone.lighttheworld;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.josephcatrambone.lighttheworld.actors.Light;
import com.josephcatrambone.lighttheworld.states.GameStateManager;
import com.josephcatrambone.lighttheworld.states.LevelEditState;
import com.josephcatrambone.lighttheworld.states.PlayState;
import com.josephcatrambone.lighttheworld.states.TitleState;

public class Game extends ApplicationAdapter {
	// Game content
	public static BitmapFont font; // We want to share a global font.  Easier that way.
	public static AssetManager assetManager;
	public static GameStateManager gameStateManager;
	public static Music currentMusic;
	
	// Game progression
	public static boolean[] completedLevels;
	public static List <String> levelNames;
	public static int currentLevel = 0;
	
	@Override
	public void create () {
		// Required global structures.
		assetManager = new AssetManager();
		gameStateManager = new GameStateManager();

		// Load the assets before building the font
		loadGameAssets();
		
		// Load music, background, etc.
		font = new BitmapFont(Gdx.files.internal("font.fnt"), new TextureRegion(Game.assetManager.get("font.png", Texture.class)));
		
		// Start playing music.
		currentMusic = Game.assetManager.get("music.ogg", Music.class);
		currentMusic.play();
		currentMusic.setLooping(true);
		
		// Load level list
		Scanner scanner = new Scanner(Gdx.files.internal("levels.txt").read());
		levelNames = new ArrayList<String>();
		while(scanner.hasNext()) {
			levelNames.add(scanner.nextLine());
		}
		scanner.close();
		completedLevels = new boolean[levelNames.size()]; // TODO: Load from user prefs.
		
		// Bring us to the title
		gameStateManager.push(new TitleState());		
	}

	@Override
	public void render () {
		Game.gameStateManager.render(Gdx.graphics.getDeltaTime());
	}
	
	public void loadGameAssets() {
		Game.assetManager.load("font.png", Texture.class);
		Game.assetManager.load("title.png", Texture.class);
		Game.assetManager.load("background.png", Texture.class);
		Game.assetManager.load("music.ogg", Music.class);
		Game.assetManager.finishLoading();
	}
}
