package com.josephcatrambone.lighttheworld.actors;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class Light {
	public static final int MAX_TOUCH_DISTANCE = 30;
	public static final int COLOR_COMPONENTS = 4;
	public static final int POSITION_COMPONENTS = 2;
	public static final int NUM_RAYS = 3000;
	public static ShaderProgram shader = null;
	
	private boolean dirty = true; // Set when we're moved.
	public Vector2 position;
	private float[] color;
	private float range = 10000;
	private Mesh mesh;
	
	public boolean isDragged = false;
	
	public static final String VERT_SHADER =  
			"attribute vec2 a_position;\n" +
			"attribute vec4 a_color;\n" +			
			"uniform mat4 u_projTrans;\n" + 
			"varying vec4 vColor;\n" +			
			"void main() {\n" +  
			"	vColor = a_color;\n" +
			"	gl_Position =  u_projTrans * vec4(a_position.xy, 0.0, 1.0);\n" +
			"}";
	
	public static final String FRAG_SHADER = 
            "#ifdef GL_ES\n" +
            "precision mediump float;\n" +
            "#endif\n" +
			"varying vec4 vColor;\n" + 			
			"void main() {\n" +  
			"	gl_FragColor = vColor;\n" + 
			"}";
	
	protected static ShaderProgram createMeshShader() {
		ShaderProgram.pedantic = false;
		ShaderProgram shader = new ShaderProgram(VERT_SHADER, FRAG_SHADER);
		String log = shader.getLog();
		if (!shader.isCompiled())
			throw new GdxRuntimeException(log);		
		if (log!=null && log.length()!=0)
			System.out.println("Shader Log: "+log);
		return shader;
	}
	
	public static Vector2 brokenRayCast(float px1, float py1, float px2, float py2, float qx1, float qy1, float qx2, float qy2) {
		// Find the intercept.
		float div = py2*qx2 - px2*qy2;
		if(div == 0) { return null; } // Parallel
		
		float t = (qx2*(qy1 - py1) + qy2*(px1 - qx1)) / div;
		float s = (px2*(qy1 - py1) + py2*(px1 - qx1)) / div;
		
		if(t < 0 || t > 1) { return null; }
		if(s < 0 || s > 1) { return null; }
		
		// Intercept
		float x = px1 + t * px2;
		float y = py1 + t * py2;
		
		return new Vector2(x, y);
	}
	
	public static Vector2 rayCast(float p0_x, float p0_y, float p1_x, float p1_y, float p2_x, float p2_y, float p3_x, float p3_y) {
		float s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
		s10_x = p1_x - p0_x;
		s10_y = p1_y - p0_y;
		s32_x = p3_x - p2_x;
		s32_y = p3_y - p2_y;
		
		denom = s10_x * s32_y - s32_x * s10_y;
		if (denom == 0) { return null; } // Collinear
		boolean denomPositive = denom > 0;
		
		s02_x = p0_x - p2_x;
		s02_y = p0_y - p2_y;
		s_numer = s10_x * s02_y - s10_y * s02_x;
		if ((s_numer < 0) == denomPositive) { return null; } // No collision
		
		t_numer = s32_x * s02_y - s32_y * s02_x;
		if ((t_numer < 0) == denomPositive) { return null; } // No collision
		
		if (((s_numer > denom) == denomPositive) || ((t_numer > denom) == denomPositive)) { return null; } // No collision
		
		// Collision detected
		t = t_numer / denom;
		return new Vector2(p0_x + (t * s10_x), p0_y + (t * s10_y));
	}
	
	public Light() {
		super();
		if(Light.shader == null) {
			Light.shader = createMeshShader();
		}
		
		this.position = new Vector2(0,0);
		
		dirty = true;
		color = new float[] {1.0f, 1.0f, 1.0f, 0.8f};
		mesh = new Mesh(true, NUM_RAYS+2, 0, 
			new VertexAttribute(Usage.Position, POSITION_COMPONENTS, "a_position"),
			new VertexAttribute(Usage.Color, COLOR_COMPONENTS, "a_color")
		);
	}
	
	public void update(float deltaTime) {
		// Top is 0
		int xPos = Gdx.input.getX();
		int yPos = Gdx.graphics.getHeight() - Gdx.input.getY();
		
		float dist = (xPos-this.position.x)*(xPos-this.position.x) + (yPos-this.position.y)*(yPos-this.position.y);
		if(dist < MAX_TOUCH_DISTANCE*MAX_TOUCH_DISTANCE) {
			if(Gdx.input.isTouched() || Gdx.input.isButtonPressed(Buttons.LEFT)) {
				isDragged = true;
			} else {
				isDragged = false;
			}
		}
		
		if(isDragged) {
			this.position.x = xPos;
			this.position.y = yPos;
			this.setDirty();
		}
	}
	
	public void setColor(float r, float g, float b, float a) {
		color[0] = r;
		color[1] = g;
		color[2] = b;
		color[3] = a;
	}
	
	public Color getColor() {
		return new Color(color[0], color[1], color[2], color[3]);
	}
	
	public void setDirty() {
		this.dirty = true;
	}
	
	private float[] getLightEdges(Polygon[] obstacles) {
		float localX = this.position.x;
		float localY = this.position.y;
		float[] rayEdges = new float[2*(NUM_RAYS+1)];
		
		// Do the cheap stupid spin.  Rotate around 360 degrees, shooting rays 
		for(int i=0; i < rayEdges.length; i+=2) {
			rayEdges[i] = localX+(float)(range*Math.cos(2*Math.PI * ((float)i/(float)NUM_RAYS)));
			rayEdges[i+1] = localY+(float)(range*Math.sin(2*Math.PI * ((float)i/(float)NUM_RAYS)));
			float minRayDistance = (rayEdges[i]-localX)*(rayEdges[i]-localX) + (rayEdges[i+1]-localY)*(rayEdges[i+1]-localY);
			for(Polygon p : obstacles) {
				float[] verts = p.getTransformedVertices();
				for(int j=0; j < verts.length; j+=2) {
					// Obstacle edge
					float px1 = verts[j];
					float py1 = verts[j+1];
					float px2 = verts[(j+2)%verts.length];
					float py2 = verts[(j+3)%verts.length];
					
					// Light ray
					float qx1 = localX;
					float qy1 = localY;
					float qx2 = rayEdges[i];
					float qy2 = rayEdges[i+1];
					
					// Get intersection
					Vector2 intercept = rayCast(px1, py1, px2, py2, qx1, qy1, qx2, qy2);
					if(intercept == null) { continue; }
					float x = intercept.x;
					float y = intercept.y;
					
					// Distance to intercept
					float distToIntercept = (x-localX)*(x-localX) + (y-localY)*(y-localY);
					
					if(distToIntercept < minRayDistance) {
						rayEdges[i] = x;
						rayEdges[i+1] = y;
						minRayDistance = distToIntercept;
					}
				}
			}
		}
		
		return rayEdges;
	}
	
	private void buildLightMesh(Polygon[] obstacles) {
		float[] outerEdge = getLightEdges(obstacles);
		int numVerts = outerEdge.length/POSITION_COMPONENTS;
		float[] verts = new float[outerEdge.length + numVerts*COLOR_COMPONENTS + (COLOR_COMPONENTS + POSITION_COMPONENTS)]; // Extra for the center.
		int i=0;
		
		// Define center
		verts[i++] = this.position.x;
		verts[i++] = this.position.y;
		verts[i++] = this.color[0];
		verts[i++] = this.color[1];
		verts[i++] = this.color[2];
		verts[i++] = this.color[3];
		
		for(int j=0; j < outerEdge.length; j+=2) {
			verts[i++] = outerEdge[j];
			verts[i++] = outerEdge[j+1];
			verts[i++] = color[0];
			verts[i++] = color[1];
			verts[i++] = color[2];
			verts[i++] = color[3];
		}
		
		this.mesh.setVertices(verts);
	}
	
	public void drawLight(Polygon[] obstacles) {
		if(dirty) { buildLightMesh(obstacles); }
		mesh.render(shader, GL20.GL_TRIANGLE_FAN, 0, this.mesh.getNumVertices());
	}
	
	public void dispose() {
		if(this.mesh != null) {
			this.mesh.dispose();
		}
	}
}
