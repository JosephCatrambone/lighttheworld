package com.josephcatrambone.lighttheworld.states;

public abstract class GameState {
	public abstract void create();
	public abstract void render(float deltaTime);
	public abstract void dispose();
}
