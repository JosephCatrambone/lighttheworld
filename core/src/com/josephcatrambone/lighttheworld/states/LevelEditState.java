package com.josephcatrambone.lighttheworld.states;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.josephcatrambone.lighttheworld.Game;
import com.josephcatrambone.lighttheworld.LevelIO;
import com.josephcatrambone.lighttheworld.actors.Light;

public class LevelEditState extends GameState {
	public static final int MAX_SELECTION_DISTANCE = 20;
	private OrthographicCamera camera;
	private ShapeRenderer shapeRenderer;
	private List <Light> lights;
	private List <Polygon> obstacles;
	private int lightSelected = -1;
	private int polygonSelected = -1;
	private int pointSelected = -1;
	private boolean debounced = false;
	
	public LevelEditState() {
		// Do something with this
		lights = new ArrayList <Light>();
		obstacles = new ArrayList <Polygon>();
		
		Light l = new Light(); // Do this to init the shader.
	}
	
	@Override
	public void create() {
		// Load the actual level.
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		shapeRenderer = new ShapeRenderer();
	}
	
	@Override
	public void render(float deltaTime) {
		// Load/Save?
		if(Gdx.input.isKeyPressed(Keys.W)) {
			System.out.println(LevelIO.toString(lights, obstacles));
		}
		if(Gdx.input.isKeyPressed(Keys.Q)) {
			lights = new ArrayList <Light>();
			obstacles = new ArrayList <Polygon>();
			System.out.println("Dump level here:");
			Scanner scanner = new Scanner(System.in);
			LevelIO.fromScanner(scanner, lights, obstacles);
			scanner.close();
		}
		
		// Get click
		float xClick = Gdx.input.getX();
		float yClick = Gdx.graphics.getHeight() - Gdx.input.getY();
		
		// Create new stuff?
		if(lightSelected == -1 && polygonSelected == -1) {
			if(Gdx.input.isKeyPressed(Keys.L)) {
				Light l = new Light();
				l.position = new Vector2(xClick, yClick);
				lights.add(l);
				lightSelected = lights.size()-1;
				System.out.println("Added light at " + xClick + "," + yClick);
				lights.add(l);
			} else if(Gdx.input.isKeyPressed(Keys.P)) {
				Polygon p = new Polygon();
				p.setVertices(new float[]{0, 0, 10, 0, 0, 10});
				obstacles.add(p);
				polygonSelected = obstacles.size()-1;
			}
		}
		if(polygonSelected != -1 && pointSelected == -1) {
			if(Gdx.input.isKeyPressed(Keys.COMMA)) {
				float[] verts = obstacles.get(polygonSelected).getVertices();
				float[] newVerts = new float[verts.length+2];
				for(int i=0; i < verts.length; i++) { newVerts[i] = verts[i]; }
				newVerts[verts.length] = xClick - obstacles.get(polygonSelected).getX();
				newVerts[verts.length+1] = yClick - obstacles.get(polygonSelected).getY();
				obstacles.get(polygonSelected).setVertices(newVerts);
				pointSelected = verts.length;
			}
		}
		
		// Unselect stuff
		if(Gdx.input.isKeyPressed(Keys.ESCAPE) || Gdx.input.isKeyPressed(Keys.ENTER)) {
			lightSelected = -1;
			polygonSelected = -1;
			pointSelected = -1;
		}
		
		// Delete stuff
		if(Gdx.input.isKeyPressed(Keys.DEL)) {
			if(lightSelected != -1) {
				lights.remove(lightSelected);
				lightSelected = -1;
			} else if(pointSelected != -1) { // Delete point before the whole poly
				float[] verts = obstacles.get(polygonSelected).getVertices();
				float[] newVerts = new float[verts.length-2];
				int j=0;
				for(int i=0; i < verts.length; i++) {
					if(i == pointSelected) { // Skip the selected vert when copying to delete it.
						i += 2;
						continue;
					}
					newVerts[j] = verts[i];
				}
				obstacles.get(polygonSelected).setVertices(newVerts);
				pointSelected = -1;
			} else if(polygonSelected != -1) {
				obstacles.remove(polygonSelected);
				polygonSelected = -1;
			}
		}
		
		// Select stuff
		if(Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
			float nearestDistance = Float.POSITIVE_INFINITY;
			// If we have a polygon selected, pick a point.
			if(polygonSelected != -1) { // Select a point in the current polygon.
				float[] verts = obstacles.get(polygonSelected).getTransformedVertices();
				for(int i=0; i < verts.length; i += 2) {
					float distance = (verts[i]-xClick)*(verts[i]-xClick) + (verts[i+1]-yClick)*(verts[i+1]-yClick);
					if(distance < nearestDistance && distance < MAX_SELECTION_DISTANCE*MAX_SELECTION_DISTANCE) {
						pointSelected = i;
						nearestDistance = distance;
					}
				}
				if(pointSelected != -1) {
					return;
				}
			}

			// Iterate through the obstacles and get one.
			for(int i=0; i < obstacles.size(); i++) {
				Polygon p = obstacles.get(i);
				if(p.contains(xClick, yClick)) {
					polygonSelected = i;
					nearestDistance = 0;
					return;
				}
			}
			
			// Iterate through all the lights and get the nearest.
			nearestDistance = Float.POSITIVE_INFINITY;
			for(int i=0; i < lights.size(); i++) {
				Light l = lights.get(i);
				float distance = (l.position.x-xClick)*(l.position.x-xClick) + (l.position.y-yClick)*(l.position.y-yClick);
				if(distance < nearestDistance && distance < MAX_SELECTION_DISTANCE*MAX_SELECTION_DISTANCE) {
					lightSelected = i;
				}
			}
		}
		
		// Move stuff
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			if(lightSelected != -1) {
				lights.get(lightSelected).position = new Vector2(xClick, yClick);
				lights.get(lightSelected).setDirty();
			} else if(polygonSelected != -1 && pointSelected == -1) {
				obstacles.get(polygonSelected).setPosition(xClick, yClick);
				for(Light l : lights) { l.setDirty(); }
			} else if(polygonSelected != -1 && pointSelected != -1) {
				float[] verts = obstacles.get(polygonSelected).getVertices();
				verts[pointSelected] = xClick - obstacles.get(polygonSelected).getX();
				verts[pointSelected+1] = yClick - obstacles.get(polygonSelected).getY();
				obstacles.get(polygonSelected).setVertices(verts);
				for(Light l : lights) { l.setDirty(); }
			}
		}
		
		// Change lights
		if(lightSelected != -1) {
			Color c = lights.get(lightSelected).getColor();
			boolean lightModified = false;
			// RED
			if(Gdx.input.isKeyPressed(Keys.A)) {
				lights.get(lightSelected).setColor(c.r+(0.1f*deltaTime), c.g, c.b, c.a);
				lightModified = true;
			}
			else if(Gdx.input.isKeyPressed(Keys.Z)) {
				lights.get(lightSelected).setColor(c.r-(0.1f*deltaTime), c.g, c.b, c.a);
				lightModified = true;
			}
			// GREEN
			else if(Gdx.input.isKeyPressed(Keys.S)) {
				lights.get(lightSelected).setColor(c.r, c.g+(0.1f*deltaTime), c.b, c.a);
				lightModified = true;
			}
			else if(Gdx.input.isKeyPressed(Keys.X)) {
				lights.get(lightSelected).setColor(c.r, c.g-(0.1f*deltaTime), c.b, c.a);
				lightModified = true;
			}
			// BLUE
			else if(Gdx.input.isKeyPressed(Keys.D)) {
				lights.get(lightSelected).setColor(c.r, c.g, c.b+(0.1f*deltaTime), c.a);
				lightModified = true;
			}
			else if(Gdx.input.isKeyPressed(Keys.C)) {
				lights.get(lightSelected).setColor(c.r, c.g, c.b-(0.1f*deltaTime), c.a);
				lightModified = true;
			}
			// BLUE
			else if(Gdx.input.isKeyPressed(Keys.F)) {
				lights.get(lightSelected).setColor(c.r, c.g, c.b, c.a+(0.1f*deltaTime));
				lightModified = true;
			}
			else if(Gdx.input.isKeyPressed(Keys.V)) {
				lights.get(lightSelected).setColor(c.r, c.g, c.b, c.a-(0.1f*deltaTime));
				lightModified = true;
			}
			
			if(lightModified) {
				lights.get(lightSelected).setDirty();
				c = lights.get(lightSelected).getColor();
				lights.get(lightSelected).setColor(
					Math.max(0, Math.min(1, c.r)), 
					Math.max(0, Math.min(1, c.g)), 
					Math.max(0, Math.min(1, c.b)), 
					Math.max(0, Math.min(1, c.a))
				);
			}
		}
		
		// Do graphics
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glDepthMask(false);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		camera.update();
		Light.shader.begin();
		Light.shader.setUniformMatrix("u_projTrans", camera.combined);
		for(Light l : lights) {
			l.drawLight(obstacles.toArray(new Polygon[]{}));
		}
		Light.shader.end();
		
		// Done drawing lights.  Draw the sprites and shapes
		// Draw the light sprites
		shapeRenderer.begin(ShapeType.Filled);
		for(Light l : lights) {
			shapeRenderer.setColor(l.getColor());
			shapeRenderer.circle(l.position.x, l.position.y, Light.MAX_TOUCH_DISTANCE);
		}
		shapeRenderer.end();
		
		// Draw outlines
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(0, 0, 0, 1.0f);
		for(Light l : lights) {
			shapeRenderer.circle(l.position.x, l.position.y, Light.MAX_TOUCH_DISTANCE);
		}
		// Draw the polygon outlines in this batch, too
		shapeRenderer.setColor(0.5f, 0, 0, 1.0f);
		for(Polygon p : obstacles) {
			shapeRenderer.polygon(p.getTransformedVertices());
		}
		shapeRenderer.end();
		
	}

	@Override
	public void dispose() {
		
	}

}
