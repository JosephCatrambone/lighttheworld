package com.josephcatrambone.lighttheworld.states;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.josephcatrambone.lighttheworld.Game;
import com.josephcatrambone.lighttheworld.LevelIO;
import com.josephcatrambone.lighttheworld.actors.Light;

public class PlayState extends GameState {
	private String levelName;
	private OrthographicCamera camera;
	private ShapeRenderer shapeRenderer;
	private SpriteBatch spriteBatch; // Needed for drawing the font.
	private Light[] lights;
	private Polygon[] obstacles;
	private boolean lightDraggedLastFrame = false;
	private boolean hasWon = false;
	
	// Purely graphical presentation stuff
	private float fadeAmount = 1.0f;
	private float fadeTarget = 0.0f;
	private float titleOffsetAmount = 0.0f;
	private float titleChangeDelayTimer = 2.0f;
	private static final float TITLE_Y_POSITION = Gdx.graphics.getHeight()*0.7f;
	
	public PlayState(String levelName) {
		// Do something with this
		this.levelName = levelName;
		Light light = new Light(); // To init shader
	}
	
	@Override
	public void create() {
		// Load the actual level.
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		shapeRenderer = new ShapeRenderer();
		spriteBatch = new SpriteBatch();
		
		// Load everything
		Scanner scanner = new Scanner(Gdx.files.internal(levelName).read());
		ArrayList <Light> tempLights = new ArrayList<Light>();
		ArrayList <Polygon> tempObstacles = new ArrayList<Polygon>();
		LevelIO.fromScanner(scanner, tempLights, tempObstacles);
		scanner.close();
		
		lights = tempLights.toArray(new Light[]{});
		obstacles = tempObstacles.toArray(new Polygon[]{});
	}

	@Override
	public void render(float deltaTime) {
		// Do logic
		boolean lightDragged = false;
		for(Light l : lights) {
			l.update(deltaTime);
			if(l.isDragged) {
				lightDragged = true;
				break;
			}
		}
		
		// Do graphics
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glDepthMask(false);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		camera.update();
		Light.shader.begin();
		Light.shader.setUniformMatrix("u_projTrans", camera.combined);
		for(Light l : lights) {
			l.drawLight(obstacles);
		}
		Light.shader.end();
		
		// Draw the light sprites
		shapeRenderer.begin(ShapeType.Filled);
		for(Light l : lights) {
			shapeRenderer.setColor(l.getColor());
			shapeRenderer.circle(l.position.x, l.position.y, Light.MAX_TOUCH_DISTANCE);
		}
		shapeRenderer.end();
		
		// Draw outlines
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(0, 0, 0, 1.0f);
		for(Light l : lights) {
			shapeRenderer.circle(l.position.x, l.position.y, Light.MAX_TOUCH_DISTANCE);
		}
		shapeRenderer.setColor(1.0f, 0, 0, 1.0f);
		for(Polygon p : obstacles) {
			shapeRenderer.polygon(p.getTransformedVertices());
		}
		shapeRenderer.end();
		
		// Debug shape render
		//Gdx.gl.glBlendFunc(GL20.GL_ONE, GL20.GL_ZERO);
		//shapeRenderer.begin(ShapeType.Point);
		//shapeRenderer.setColor(0f, 1.0f, 1.0f, 1.0f);
		// Check if we've won.
		if(lightDraggedLastFrame && !lightDragged && !hasWon) {
			if(completed()) {
				System.out.println("Won level!");
				fadeTarget = 1.0f;
				hasWon = true;
			}
		}
		lightDraggedLastFrame = lightDragged;
		shapeRenderer.end();

		// Draw the intro text
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
		//spriteBatch.enableBlending();
		Game.font.draw(spriteBatch, levelName, Gdx.graphics.getWidth()/2 - Game.font.getBounds(levelName).width/2, TITLE_Y_POSITION+titleOffsetAmount);
		spriteBatch.end();
		titleChangeDelayTimer -= deltaTime;
		if(titleChangeDelayTimer <= 0) {
			titleOffsetAmount += (3*titleOffsetAmount+1000f)/4f*deltaTime;
		}
		Gdx.gl.glEnable(GL20.GL_BLEND); // Reenable blending at the end.
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		// Draw the screen fade
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(1.0f, 1.0f, 1.0f, fadeAmount);
		shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		shapeRenderer.end();
		
		// Check if we've finished the fade and, if so, advance to the next level.
		if(Math.abs(fadeTarget-fadeAmount) > 0.0001f) { // We calculate the fade afterwards because we want the nextLevel/dispose to be the LAST thing we do.
			fadeAmount += (10*deltaTime)*(fadeTarget-fadeAmount)/2.0f;
		} else if(hasWon) {
			nextLevel();
		}
	}
	
	public boolean completed() {
		// TODO: Raycast from every _object_ corner to at least one light.
		int validPoints = 0;
		int illuminatedPoints = 0;
		for(int y=0; y < Gdx.graphics.getHeight(); y += 5) {
			for(int x=0; x < Gdx.graphics.getWidth(); x += 5) {
				// If this point is inside a polygon, don't use it.
				boolean valid = true;
				for(Polygon p : obstacles) {
					if(p.contains(x, y)) { valid = false; break; }
				}
				if(!valid) { continue; }
				validPoints++;
				
				// Okay, this isn't inside a polygon.  Does it see a light?
				boolean seesLight = false;
				for(int lightNum = 0; lightNum < lights.length && !seesLight; lightNum++) {
					seesLight = true;
					Light l = lights[lightNum];
					for(Polygon p : obstacles) {
						float[] verts = p.getVertices();
						for(int i=0; i < verts.length; i+=2) {
							if(Light.rayCast(x, y, l.position.x, l.position.y, verts[i], verts[i+1], verts[(i+2)%verts.length], verts[(i+3)%verts.length]) != null) {
								seesLight = false;
							}
						}
					}
				}
				if(seesLight) {
					illuminatedPoints++;
				} else {
					//shapeRenderer.point(x, y, 0);
				}
			}
		}
		System.out.println(((float)illuminatedPoints/(float)validPoints) + "% of points illuminated");
		if(((float)illuminatedPoints/(float)validPoints) > 0.9995) { 
			return true;
		} else {
			return false;
		}
	}
	
	public void nextLevel() {
		Game.gameStateManager.pop();
		Game.currentLevel++;
		if(Game.currentLevel < Game.levelNames.size()) {
			Game.gameStateManager.push(new PlayState(Game.levelNames.get(Game.currentLevel)));
		}
		this.dispose();
	}

	@Override
	public void dispose() {
		for(Light l : lights) {
			l.dispose();
		}
		shapeRenderer.dispose();
	}

}
