package com.josephcatrambone.lighttheworld.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.josephcatrambone.lighttheworld.Game;

public class TitleState extends GameState {

	Texture titleScreen = null;
	SpriteBatch batch = null;
	
	@Override
	public void create() {
		batch = new SpriteBatch();
		titleScreen = Game.assetManager.get("title.png", Texture.class);
	}

	@Override
	public void render(float deltaTime) {
		if(Gdx.input.isTouched()) {
			Game.gameStateManager.push(new LevelSelectState());
		} else if(Gdx.input.isKeyPressed(Keys.TAB)) {
			Game.gameStateManager.push(new LevelEditState());
		}
		
		batch.begin();
		batch.draw(titleScreen, Gdx.graphics.getWidth()/2 - titleScreen.getWidth()/2, Gdx.graphics.getHeight()/2 - titleScreen.getHeight()/2);
		batch.end();
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

}
