package com.josephcatrambone.lighttheworld.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.josephcatrambone.lighttheworld.Game;

public class LevelSelectState extends GameState {

	private BitmapFont font;
	private ShapeRenderer shapeRenderer;
	
	@Override
	public void create() {
		
	}

	@Override
	public void render(float deltaTime) {
		if(Gdx.input.isTouched()) {
			Game.gameStateManager.push(new PlayState(Game.levelNames.get(Game.currentLevel)));
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
