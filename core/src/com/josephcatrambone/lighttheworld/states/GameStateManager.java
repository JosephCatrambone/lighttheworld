package com.josephcatrambone.lighttheworld.states;

import java.util.Stack;

public class GameStateManager extends Stack<GameState> {
	private static final long serialVersionUID = -7176057834412911217L;

	@Override
	public GameState push(GameState gs) {
		gs.create();
		super.push(gs);
		return gs;
	}
	
	public void render(float deltaTime) {
		this.peek().render(deltaTime);
	}
	
	public void dispose() {
		while(!this.isEmpty()) {
			this.peek().dispose();
			this.pop();
		}
	}
}
